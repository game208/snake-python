import pygame
from random import randrange

# размер окна
RES = 800
SIZE = 50

#  спавн змейки и яблока
x, y = randrange(0, RES, SIZE), randrange(0, RES, SIZE)
apple = randrange(0, RES, SIZE), randrange(0, RES, SIZE)
dirs = {'W': True, 'S': True, 'A': True, 'D': True}
length = 1  # длина змейки
snake = [(x, y)]
dx, dy = 0, 0  # направление движения
fps = 2  # скорость змейки
score = 0  # счет игры

# создание рабочего окна
pygame.init()
sc = pygame.display.set_mode([RES, RES])
clock = pygame.time.Clock()
font_score = pygame.font.SysFont('Arial', 26, bold=True)
font_end = pygame.font.SysFont('Arial', 66, bold=True)
img = pygame.image.load('1.jpg').convert()
pygame.display.set_caption("Snake")

while True:
    sc.blit(img, (-550, -200))
    # отображение змейки, яблока
    [(pygame.draw.rect(sc, pygame.Color('Green'), (i, j, SIZE - 1, SIZE - 1))) for i, j in snake]
    pygame.draw.rect(sc, pygame.Color('red'), (*apple, SIZE, SIZE))
    render_score = font_score.render(f'SCORE: {score}', 1, pygame.Color('orange'))
    sc.blit(render_score, (5, 5))

    # движение змейки
    x += dx * SIZE
    y += dy * SIZE
    snake.append((x, y))
    snake = snake[-length:]
    # кушает яблоко
    if snake[-1] == apple:
        apple = randrange(0, RES, SIZE), randrange(0, RES, SIZE)
        length += 1
        fps += 1
        score += 1
    # конец игры
    if x < 0 or x > RES - SIZE or y < 0 or y > RES - SIZE or len(snake) != len(set(snake)):
        while True:
            render_end = font_end.render('GAME OVER', 1, pygame.Color('red'))
            sc.blit(render_end, (RES // 2 - 200, RES // 3))
            pygame.display.flip()


    # задержка для FPS
    pygame.display.flip()
    clock.tick(fps)

    # закрытие окна
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()

    # управление движением
    key = pygame.key.get_pressed()
    if key[pygame.K_w] and dirs['W']:
        dx, dy = 0, -1
        dirs = {'W': True, 'S': False, 'A': True, 'D': True}
    if key[pygame.K_s] and dirs['S']:
        dx, dy = 0, 1
        dirs = {'W': False, 'S': True, 'A': True, 'D': True}
    if key[pygame.K_a] and dirs['A']:
        dx, dy = -1, 0
        dirs = {'W': True, 'S': True, 'A': True, 'D': False}
    if key[pygame.K_d] and dirs['D']:
        dx, dy = 1, 0
        dirs = {'W': True, 'S': True, 'A': False, 'D': True}
